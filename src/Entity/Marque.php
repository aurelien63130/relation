<?php

namespace App\Entity;

use App\Repository\MarqueRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MarqueRepository::class)]
class Marque
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $nom;

    #[ORM\OneToMany(targetEntity: Produit::class, mappedBy: 'marque')]
    private $products;


    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function addProduct(Produit $produit){
        if(!$this->products->contains($produit)){
            $this->products->add($produit);
        }
    }

    public function removeProduct(Produit $produit){
        if($this->products->contains($produit)){
            $this->products->removeElement($produit);
        }
    }

    public function getProducts(){
        return $this->products;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }


}
