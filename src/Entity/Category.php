<?php
namespace App\Entity;


use App\Repository\CategoryRepository;
use App\Repository\MarqueRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CategoryRepository::class)]
class Category
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $nom;

    #[ORM\OneToMany(targetEntity: Category::class, mappedBy: 'parent')]
    private $childrens;

    #[ORM\ManyToOne(targetEntity: Category::class, inversedBy: 'childrens')]
    private $parent;

    public function __construct(){
        $this->childrens = new ArrayCollection();
    }

    public function addChild(Category $category){
        if(!$this->childrens->contains($category)){
            $this->childrens->add($category);
        }
    }

    public function removeChild(Category $category){
        if($this->childrens->contains($category)){
            $this->childrens->removeElement($category);
        }
    }

    public function getChildren(){
        return $this->childrens;
    }

}
