<?php

namespace App\Controller;

use App\Entity\Marque;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MarqueController extends AbstractController
{
    #[Route('/marque/{id}', name: 'marque')]
    public function index(Marque $marque): Response
    {
        return $this->render('marque/index.html.twig', [
            'marque' => $marque,
        ]);
    }
}
